all: main

# run in your devel vm
main: main.c test.o
	gcc -g main.c test.o -o main

test.o: test.S
	gcc -g -c test.S -o test.o


# Use your own compiler in your own machine (Windows/Ubuntu/Mac)
# I haven't tested mac since I don't have one :)
# main: main.c test.o
# 	gcc -m32 -g main.c test.o -o main

# test.o: test.S
# 	gcc -m32 -g -c test.S -o test.o

clean:
	rm *.o
	rm main

