# Intro

Use this project as a very basic starter code of x86. If you have a 32bit virtual machine like the one used in UIUC ECE391, you can directly run `make` to get the exectuable `main`. You can test all the grammar of x86 in file `test.S`.

## About makefile
If you have a 64bit machine/VM/WSL, add `-m32` flag to tell `gcc` to compile the code for 32bit architecture. If you have a 32bit VM like the one we use in ECE391, you don't need to change anything.

For Ubuntu 64bit users, installing `build-essential` will only install 64bit compilers for you. You can use the following command to install the support of 32bit compilers.

```
sudo apt install gcc-multilib g++-multilib
```

For Windows users, if you are using Windows MinGW, please go to `test.S` and change the `test_asm` to `_test_asm` because of a Windows platform mechanism called "Name Mangling".

```
.text
.global _test_asm

_test_asm:
    <the rest of the code remains the same>
```

## Use gdb to learn x86
Start gdb to debug `main`.
```
gdb main
```

You can set the break point to check the value of registers. For example, if I want to know what is going on in 
`
movl $label, %eax
`
, I can first set the break point.
```
break test.S:13
```
Then we can run the code to hit it.
```
run
```
Then we trigger the next line of code by `step` or `s`.
```
step
```


At this point, we can examine the value of registers, so use the following command to print out the eax. Note that `p/x` means print in hex format, and `$eax` is the gdb grammar to indicate a register. You can also use `info reg` to get a list of register values. 
```
p/x $eax
```
Step through the rest of lines and think about the reason why `p/x $eax` gives this.

As for the `label` in the `test.S`, it can be seen as a variable called `label` whose type is `long`. 
Therefore we can check its address in gdb. The `&label` is just like the grammar we use in C.
```
p/x &label
```
We can also check its value in gdb (please specify its type).
```
p (long)label
```

## Illegal x86 command
In `test.S`, line 18 is illegal, uncomment it and execute `make` again to check out what happens.